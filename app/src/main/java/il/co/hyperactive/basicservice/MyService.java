package il.co.hyperactive.basicservice;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

public class MyService extends IntentService {
    int i;

    public MyService(String name) {
        super(name);
    }
    public MyService()
    {
        super("MyService");
    }
    Handler handler = new Handler();
    @Override
    protected void onHandleIntent(Intent intent) {
        for(i = 1;i<5;i++)
        {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MyService.this, "Run number " + i, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
